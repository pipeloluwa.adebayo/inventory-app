const express = require('express');
const bodyParser = require('body-parser');
const res = require('express/lib/response');
const port = 3000;

const app = express();

app.use(bodyParser.json())

const products = [];
const users = [];

class Product {
    constructor(id, 
        name, description) {
        this.name = name;
        this.description = description;
        this.id = id;  
        products.push(this)
};
}
const product1 = new Product(1, "sunscreen", "all skin types")
const product2 = new Product(2, "serum", "for sensitive skin")

class User {
    constructor(id, name, email, role){
        this.id = id;
        this.name = name;
        this.email = email;
        this.role = role;
        users.push(this)
    };
}
//const user1 = new User(01, "anon", "a@anon.com", "admin")


//users
app.get('/', (req, res) => {
    res.send("Hello peeps")
});

app.post('/product', (req, res) => {
    const bodyOfRequest = req.body;

    if (!bodyOfRequest.name || !bodyOfRequest.name.length) {
        const obj = {
            "error": 'no name in this request',
            "data": null
        }
        return res.status(400).json(obj)
    }

    if (!bodyOfRequest.description || !bodyOfRequest.description.length) {
        const obj = {
            "error": 'no description in this request',
            "data": null
        }
        return res.status(400).json(obj)
    }


    const productIndb = products.find((item) => item.name === bodyOfRequest.name);

    if (productIndb) {
        return res.status(400).json({
            error: 'producnt already exist',
            data: null
        })
    }

    const product = new Product(bodyOfRequest.name, bodyOfRequest.description)
    products.push(product);

    res.status(200).json({
        error: null,
        data: product,
    })
})

app.get('/product/:name', (req, res) => {
    const params = req.params;

    const product = products.find((item) => item.name === params.name);

    if (!product) {
        return res.status(400).json({
            error: 'not found',
            data: null
        })
    }

    const returnedObj = { error: null, data: product }

    res.json(returnedObj)
})

app.get('/products', (req, res) => {
    return res.status(200).json({ error: null, data: products })
})


app.put('/products/:id', (req, res) => {
    const getProduct = products.some(product => product.id === parseInt(req.params.id));
    if(getProduct) {
        const updateProduct = req.body;

        products.forEach(product => {
            if(product.id === parseInt(req.params.id)) {
                product.name = updateProduct.name ? updateProduct.name : product.name;
                product.description = updateProduct.description
                res.json({
                    msg: "product successfully updated", product
                })
            }
        });
    } else {
        return res.status(400).json({
            error: 'not found',
            data: null
        })
    }
});


app.delete('/products/:id', (req, res) => {
    const getProduct = products.some(product => product.id === parseInt(req.params.id));
    
    if(getProduct){
        products = products.filter(product => product.id === parseInt(req.params.id));
        res.json({msg: "user successfully deleted "});
    } else{
        return res.status(400).json({
            error: 'unsuccessful',
            data: null
        })
    }

});

// users
app.get('/users', (req, res) => {
    return res.status(200).json({ error: null, data: users }) //get all users

});

app.get('/users/:id', (req, res) => {
    const getUser = users.some(user => user.id === parseInt(req.params.id));
     if(getUser){
         res.json(users.filter(user => user.id === parseInt(req.params.id)));
     } else {
        return res.status(400).json({
            error: 'not found',
            data: null
        })
     }
});

app.post('/user', (req, res) => {
    const newUser = {
        id: req.body.id,
        name: req.body.name,
        email: req.body.email,
        role: req.body.role
    };

    if(!newUser.name || !newUser.email){
        return res.status(400).json({
            error: 'unsuccessful',
            data: null
        })
    }

    users.push(newUser);
    res.json(users);
})

app.put('/users/:id', (req, res) => {
    const getUser = users.some(user => user.id === parseInt(req.params.id));
    if(getUser) {
        const updateUser = req.body;

        users.forEach(user => {
            if(user.id === parseInt(req.params.id)) {
                user.name = updateUser.name ? updateUser.name : user.name;
                user.email = updateUser.email
                res.json({
                    msg: "user successfully updated", user
                })
            }
        });
    } else {
        return res.status(400).json({
            error: 'not found',
            data: null
        })
    }
});



app.delete('/users/:id', (req, res) => {
    const getUser = users.some(user => user.id === parseInt(req.params.id));
    
    if(getUser){
        users = users.filter(user => user.id === parseInt(req.params.id));
        res.json({msg: "user successfully deleted "});
    } else{
        return res.status(400).json({
            error: 'not found',
            data: null
        })
    }

});





app.listen(port, () => console.log('App listening on port', port))
